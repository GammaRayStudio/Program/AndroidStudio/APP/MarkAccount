Android App 開發 : 用 Markdown 記帳的 App
======
`2021-11-30`

目前進度
------

    結合 FileUtil 與 RecyclerView，
    實作 「檔案瀏覽器」的功能。
    
今日任務
------
	• 整併 開發文件 
    • 導航列與檔案列表 定位當前路徑
    • 檔案列表 開啟 檔案
    • 檔案列表 開啟 資料夾 
    • 檔案列表 返回上一頁 
    • 導航列 切換路徑
    • 新增 資料夾 功能 (按鈕 與 Dialog)
    • 新增 檔案 功能 (按鈕 與 Dialog)


+ [完整進度: to-do]
[完整進度: to-do]:https://gitlab.com/GammaRayStudio/Program/AndroidStudio/DOC/MarkAccount-DOC/-/blob/master/001.mark-account-to_do.md

<br>

產品程式
------
### MarkAccount 
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/APP/MarkAccount>

<br>


開發文件
------
### MarkAccount-DOC
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/DOC/MarkAccount-DOC>

<br>


工具專案
------
### JavaProjUtil 
<https://gitlab.com/GammaRayStudio/Program/JavaStudio/SE/JavaProjUtil>


### 工具專案 : AndroidProjUI
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/SE/AndroidProjUI>

### 工具專案 : AndroidFileBrowser
<https://gitlab.com/GammaRayStudio/Program/AndroidStudio/SE/AndroidFileBrowser>



